import tensorflow as tf
import numpy as np
from six.moves import cPickle as pickle
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
import itertools
import os
import sys


def reformat(dataset, labels):
    dataset = dataset.reshape(
        -1, image_size, image_size, num_channels)

    # Convert labels to one hot encoding
    labels = (np.arange(num_labels) == labels[:, None]).astype(np.float32)
    return dataset, labels


def accuracy(predictions, labels):
  return (100.0 * np.sum(np.argmax(predictions, 1) == np.argmax(labels, 1))
          / predictions.shape[0])


def showImage(data):
    data = data.reshape(image_size, image_size)
    plt.imshow(data)
    plt.show()


if len(sys.argv) != 3:
    print("Wrong parameters")
    exit()

input_dir = sys.argv[1]
preprocess_flag = sys.argv[2]
if not os.path.exists(input_dir):
    print("Pickle directory not found")
    exit()

pickle_file = input_dir + "/facial_reg_{}.pickle".format(preprocess_flag)
if not os.path.exists(pickle_file):
    print("Pickle file not exists")
    exit()

with open(pickle_file, 'rb') as f:
    try:
        save = pickle.load(f)
        train_dataset = save['train_dataset']
        train_labels = save['train_labels']
        valid_dataset = save['valid_dataset']
        valid_labels = save['valid_labels']
        test_dataset = save['test_dataset']
        test_labels = save['test_labels']
        del save  # hint to help gc free up memory
        print('Training set', train_dataset.shape, train_labels.shape)
        print('Validation set', valid_dataset.shape, valid_labels.shape)
        print('Test set', test_dataset.shape, test_labels.shape)
    except:
        print("Unable to load file")
        exit()

image_size = 48
num_labels = 7
num_channels = 1 # grayscale
train_dataset, train_labels = reformat(train_dataset, train_labels)
valid_dataset, valid_labels = reformat(valid_dataset, valid_labels)
test_dataset, test_labels = reformat(test_dataset, test_labels)
#showImage(train_dataset[0])

batch_size = 800
patch_size = 5
depth = 64
num_hidden = 128

graph = tf.Graph()

with graph.as_default():

    # Input data
    tf_train_dataset = tf.placeholder(
        tf.float32, shape=(batch_size, image_size, image_size, num_channels))
    tf_train_labels = tf.placeholder(tf.float32, shape=(batch_size, num_labels))
    tf_valid_dataset = tf.placeholder(tf.float32, shape=(batch_size, image_size, image_size, num_channels))
    tf_test_dataset = tf.placeholder(tf.float32, shape=(batch_size, image_size, image_size, num_channels))

    # Drop out holder
    keep_prob = tf.placeholder(dtype=tf.float32)

    # Variables
    layer1_weights = tf.get_variable("layer1_weights", shape=[patch_size, patch_size, num_channels, depth],
                                     initializer=tf.random_normal_initializer(stddev=0.1))
    layer1_biases = tf.get_variable("layer1_biases", shape=[depth], initializer=tf.constant_initializer(0))

    layer2_weights = tf.get_variable("layer2_weights", shape=[patch_size, patch_size, depth, num_hidden],
                                     initializer=tf.random_normal_initializer(stddev=0.1))
    layer2_biases = tf.get_variable("layer2_biases", shape=[num_hidden], initializer=tf.constant_initializer(0))

    # Divide by 4 here, as images have been halved twice during max pooling steps
    layer3_weights = tf.get_variable("layer3_weights", shape=[image_size // 4 * image_size // 4 * num_hidden, 3100],
                                     initializer=tf.random_normal_initializer(stddev=0.1))
    layer3_biases = tf.get_variable("layer3_biases", shape=[3100], initializer=tf.constant_initializer(0))

    layer4_weights = tf.get_variable("layer4_weights", shape=[3100, 1500],
                                     initializer=tf.random_normal_initializer(stddev=0.1))
    layer4_biases = tf.get_variable("layer4_biases", shape=[1500], initializer=tf.constant_initializer(0))

    layer5_weights = tf.get_variable("layer5_weights", shape=[1500, num_labels],
                                     initializer=tf.random_normal_initializer(stddev=0.1))
    layer5_biases = tf.get_variable("layer5_biases", shape=[num_labels], initializer=tf.constant_initializer(0))

    # Model.
    def model(data):
        # Convolution in the first layer has to keep the same shape as the input so the stride is [1, 1, 1, 1]
        # and padding must be SAME to keep the conv width and height equal to width and height in the input
        conv = tf.nn.conv2d(data, layer1_weights, [1, 1, 1, 1], padding='SAME')
        hidden = tf.nn.relu(conv + layer1_biases)

        # Reduce the dimensionality by using the max pool operation here with kernel size 2 and stride 2
        #norm1 = tf.nn.lrn(hidden, depth_radius=4, bias=1.0, alpha=0.001/9.0, beta=0.75)
        maxpool = tf.nn.max_pool(hidden, [1, 2, 2, 1], [1, 2, 2, 1], padding='SAME')
        #maxpool = tf.nn.lrn(maxpool, depth_radius=4, bias=1.0, alpha=0.001/9.0, beta=0.75)

        conv = tf.nn.conv2d(maxpool, layer2_weights, [1, 1, 1, 1], padding='SAME')
        hidden = tf.nn.relu(conv + layer2_biases)

        # Add drop out here
        #hidden_dropout = tf.nn.dropout(hidden, keep_prob)
        #hidden = tf.nn.lrn(hidden, depth_radius=4, bias=1.0, alpha=0.001/9.0, beta=0.75)
        hidden = tf.nn.max_pool(hidden, [1, 2, 2, 1], [1, 2, 2, 1], padding='SAME')
        #hidden = tf.nn.lrn(hidden, depth_radius=4, bias=1.0, alpha=0.001/9.0, beta=0.75)

        shape = hidden.get_shape().as_list()

        # Fully connected layer, batch_size x total_features
        # Rollout height, width and feature_map into total features
        reshape = tf.reshape(hidden, [shape[0], shape[1] * shape[2] * shape[3]])
        hidden1 = tf.nn.relu(tf.matmul(reshape, layer3_weights) + layer3_biases)
        hidden2 = tf.nn.relu(tf.matmul(hidden1, layer4_weights) + layer4_biases)
        hidden2 = tf.nn.dropout(hidden2, keep_prob)
        return tf.matmul(hidden2, layer5_weights) + layer5_biases

    # Training computation.
    logits = model(tf_train_dataset)
    #loss = -tf.reduce_sum(tf_train_labels*tf.log(tf.nn.softmax(logits)))
    #loss = tf.reduce_sum(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=tf_train_labels))
    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=tf_train_labels))

    # Just a number of time the model update the weight which means the number of batches the optimizer
    # has seen
    #global_step = tf.get_variable("global_step", initializer=tf.constant(0))
    global_step = tf.Variable(0, trainable=False, name="global_step")
    #tf.add_to_collection("global_step", global_step)

    # Add learning decay here
    # Here is the decay learning rate formula:
    # decayed_learning_rate = learning_rate * decay_rate ^ (global_step / decay_steps)
    learning_rate = tf.train.exponential_decay(learning_rate=1e-4, global_step=global_step, decay_rate=0.96,
                                               decay_steps=100000, staircase=True)
    #learning_rate = 1e-4
    saver = tf.train.Saver(max_to_keep=5)

    # Optimizer.
    #optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss, global_step=global_step)
    optimizer = tf.train.AdamOptimizer(learning_rate).minimize(loss, global_step=global_step)
    #optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss)

    # Predictions for the training, validation, and test data.
    train_prediction = tf.nn.softmax(logits)
    valid_prediction = tf.nn.softmax(model(tf_valid_dataset))
    test_prediction = tf.nn.softmax(model(tf_test_dataset))


num_steps = 1000
training_accuracies = []
validation_accuracies = []
test_prediction_labels = None
x_range = []
step_per_checkpoint = 100
train_dir = "Checkpoints"
test_batch_range = 0

if not os.path.exists(train_dir):
    print("Creating training directory")
    os.makedirs(train_dir)


with tf.Session(graph=graph) as session:
    session.run(tf.global_variables_initializer())
    ckpt = tf.train.get_checkpoint_state(train_dir)
    if ckpt and tf.train.checkpoint_exists(ckpt.model_checkpoint_path):
        saver.restore(session, tf.train.latest_checkpoint(train_dir))
        print("Loading from checkpoint %d" % global_step.eval())
    else:
        print('Initializing with fresh parameters')
    for step in range(0, num_steps):
        offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
        batch_data = train_dataset[offset:(offset + batch_size), :, :, :]
        batch_labels = train_labels[offset:(offset + batch_size), :]
        feed_dict = {keep_prob: 0.5, tf_train_dataset: batch_data, tf_train_labels: batch_labels}
        _, l, predictions = session.run(
          [optimizer, loss, train_prediction], feed_dict=feed_dict)
        if global_step.eval() % 50 == 0:
            print("Minibatch loss at step %d: %f" % (global_step.eval(), l))
            train_accuracy = accuracy(train_prediction.eval(feed_dict={keep_prob: 1.0, tf_train_dataset: batch_data}),
                                      batch_labels)
            training_accuracies.append(train_accuracy)
            print("Minibatch accuracy: %.1f%%" % train_accuracy)

            val_accuracy = 0
            offset = 0
            val_batch_range = int(valid_labels.shape[0] / batch_size)
            for val_step in range(0, val_batch_range):
                # We have to calculate accuracy on small batch to prevent GPU memory exhaust
                val_batch_data = valid_dataset[offset:(offset + batch_size), :, :, :]
                val_batch_labels = valid_labels[offset:(offset + batch_size), :]

                # In validation and test sets we do not add drop out so keep_prob will be 1
                val_accuracy += accuracy(valid_prediction.eval(feed_dict={keep_prob: 1.0,
                                                                         tf_valid_dataset: val_batch_data}), val_batch_labels)
                offset += batch_size
            val_accuracy /= val_batch_range
            validation_accuracies.append(val_accuracy)
            print('Validation accuracy: %.1f%%' % val_accuracy)
            x_range.append(global_step.eval())
        if global_step.eval() % step_per_checkpoint == 0:
            print("Saving model at %d steps" % global_step.eval())
            checkpoint_path = os.path.join(train_dir, "cnnmodel")
            saver.save(session, checkpoint_path, global_step=global_step.eval(), write_meta_graph=False)

    test_batch_range = int(test_labels.shape[0] / batch_size)
    test_accuracy = 0
    offset = 0
    for test_step in range(0, test_batch_range):
        test_batch_data = test_dataset[offset:(offset + batch_size), :, :, :]
        test_batch_labels = test_labels[offset:(offset + batch_size), :]
        test_pred = test_prediction.eval(feed_dict={keep_prob: 1.0, tf_test_dataset: test_batch_data})
        if test_prediction_labels is None:
            test_prediction_labels = test_pred
        else:
            test_prediction_labels = np.concatenate((test_prediction_labels, test_pred), axis=0)
        test_accuracy += accuracy(test_pred, test_batch_labels)
    test_accuracy /= test_batch_range
    print('Test accuracy: %.1f%%' % test_accuracy)


def plotAccuracies(train_acc, val_acc, x_range):
    plt.plot(x_range, train_acc, '-b', label="Training")
    plt.plot(x_range, val_acc, '-g', label="Validation")
    plt.legend(loc="lower right", frameon=False)
    plt.ylim(ymin=0.0, ymax=100.0)
    plt.ylabel("Accuracies")
    plt.xlabel("Step")
    plt.title("Training and Validation Accuracies")
    plt.show()


def plotConfusionMatrix(cm, classes, normalize=False, title="Confusion matrix"):
    if normalize:
        print("Normalized confusion matrix")
        # This is the same as cm.astype("float") / cm.sum(axis=1).reshape(-1, 1)
        cm = cm.astype("float") / cm.sum(axis=1)[:, np.newaxis]
    else:
        print("Confusion matrix, without normalization")

    print(cm)

    # Plot the confusion matrix
    plt.imshow(cm, interpolation="nearest", cmap=plt.cm.Blues)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    threshold = cm.max() / 2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j], horizontalalignment="center", color="white" if cm[i, j] > threshold
                 else "black")
    plt.tight_layout()
    plt.ylabel("True label")
    plt.xlabel("Predicted label")
    plt.show()

plotAccuracies(training_accuracies, validation_accuracies, x_range)

# Sorted order according to the data description
class_labels = ["Angry", "Disgust", "Fear", "Happy", "Sad", "Surprise", "Neutral"]
confusionmatrix = confusion_matrix(np.argmax(test_labels[:(test_batch_range*batch_size)], axis=1),
                                   np.argmax(test_prediction_labels, axis=1))
plotConfusionMatrix(confusionmatrix, class_labels, False)

def getLabel(label):
    label_index = np.argmax(label, axis=0)
    return class_labels[label_index]

best_checkpoint = 23600
test_image = 5
best_checkpoint_path = os.path.join(train_dir, "cnnmodel-{}".format(best_checkpoint))

with tf.Session(graph=graph) as session:
    session.run(tf.global_variables_initializer())
    ckpt = tf.train.get_checkpoint_state(train_dir)
    if ckpt and tf.train.checkpoint_exists(ckpt.model_checkpoint_path):
        # Restore the best checkpoint which is 23600
        saver.restore(session, best_checkpoint_path)
        print("Loading from the checkpoint %d" % best_checkpoint)
    else:
        print("Cannot find the checkpoint %d" % best_checkpoint)
        exit()

    # Just get the first batchsize in the private test for testing
    batch_data = test_dataset[0:batch_size, :, :, :]
    batch_labels = test_labels[0:batch_size, :]
    testing_pred = test_prediction.eval(feed_dict={keep_prob: 1.0, tf_test_dataset: batch_data})
    for img in range(test_image):
        print("Showing image %d" % img)
        showImage(batch_data[img, :, :, :])
        print("True label: %s" % getLabel(batch_labels[img, :]))
        print("Predicted label: %s" % getLabel(testing_pred[img, :]))