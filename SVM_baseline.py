from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.datasets import fetch_lfw_people
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.decomposition import PCA
from sklearn.svm import SVC


df = pd.read_csv("Data/fer2013/fer2013.csv")
preprocess_flag = 0

# Train dataset
train_dataset = df[df["Usage"] == "Training"]
train_data = convertData(train_dataset.loc[:, "pixels"], preprocess_flag)
pca = PCA(n_components=150, svd_solver='randomized',
          whiten=True).fit(train_data)
train_data = pca.transform(train_data)
train_labels = train_dataset.loc[:, "emotion"].values.ravel()

# Valid dataset
valid_dataset = df[df.Usage == "PublicTest"]
valid_dataset.reset_index(inplace=True)
valid_data = convertData(valid_dataset.loc[:, "pixels"], preprocess_flag)
valid_data = pca.transform(valid_data)
valid_labels = valid_dataset.loc[:, "emotion"].values.ravel()

# Test dataset
test_dataset = df[df.Usage == "PrivateTest"]
test_dataset.reset_index(inplace=True)
test_data = convertData(test_dataset.loc[:, "pixels"], preprocess_flag)
test_data = pca.transform(test_data)
test_labels = test_dataset.loc[:, "emotion"].values.ravel()

param_grid = {'C': [1e3],
              'gamma': [0.1]}
clf = GridSearchCV(SVC(kernel='rbf', class_weight='balanced'), param_grid)
print("Training")
clf = clf.fit(train_data, train_labels)
print("Predicting")
y_pred = clf.predict(valid_data)
print(classification_report(valid_labels, y_pred))
print(confusion_matrix(valid_labels, y_pred, labels=range(7)))