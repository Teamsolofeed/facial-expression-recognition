### Nhận diện khuôn mặt người sử dụng Convolution Neural Networks###

* Chương trình sử dụng framework mã nguồn mở **Tensorflow** được phát triển bởi Google <https://www.tensorflow.org/>
* Chương trình sử dụng tập dữ liệu của một cuộc thi nhận dạng biểu cảm con người trên trang web <https://www.kaggle.com>. Thông tin chi tiết của cuộc thi <https://www.kaggle.com/c/challenges-in-representation-learning-facial-expression-recognition-challenge>
* Tập dữ liệu được tải về từ đây <https://www.kaggle.com/c/challenges-in-representation-learning-facial-expression-recognition-challenge/data>
* Chương trình sử dụng mô hình **LeNet** để học biểu cảm khuôn mặt người.
* Chương trình này chỉ mang tính chất học tập. Không dùng cho mục đích thương mại dưới mọi hình thức.

### Yêu cầu của chương trình ###

* Cài đặt python và pip
* Cài đặt framework Tensorflow theo hướng dẫn <https://www.tensorflow.org/install/>. Nếu có GPU, ta có thể sử dụng tensorflow-gpu để tăng tốc độ chương trình (Link hướng dẫn cài đặt tensorflow-gpu trên window https://www.tensorflow.org/install/install_windows )
* Pip install cái package numpy, scipy, scikit-learn

### Cách chạy chương trình ###
Đầu tiên chạy tập tin **data_util.py** để tiền xử lý dữ liệu. Chạy chương trình bằng tham số sau:

	python data_util.py <input_file> <output_dir> <preprocess_flag>
	
**Ví dụ:**

	python data_util.py Data/fer2013.csv Data 0

Sau khi đã tiền xử lý dữ liệu ta chạy chương trình **CNN_Facial_Regcognition.py** như sau:

	python CNN_Facial_Regcognition.py <input_dir> <preprocess_flag>
	
**Ví dụ:**

	python CNN_Facial_Recognition.py Data 0
	
* Thông tin chi tiết của chương trình nằm trong tập tin **FacialExpression.ipynb**

### Kết quả chương trình ###
* Chương trình cho kết quả độ chính xác ở tập PrivateTest là **55.4%**. Nằm trong top 20 của bảng xếp hạng <https://www.kaggle.com/c/challenges-in-representation-learning-facial-expression-recognition-challenge/leaderboard>

