import tensorflow as tf
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing
from six.moves import cPickle as pickle
import os
import sys

image_size = 48

# Convert array of strings to numpy matrices
def convertData(data, preprocess_flag=0):
    preprocess = None
    if preprocess_flag == 1:
        preprocess = preprocessing.StandardScaler()
    elif preprocess_flag == 2:
        preprocess = preprocessing.Normalizer()
    elif preprocess_flag == 3:
        preprocess = preprocessing.MaxAbsScaler()
    elif preprocess_flag == 4:
        preprocess_flag = preprocessing.MinMaxScaler()
    convert_data = np.ndarray(shape=(data.shape[0], image_size * image_size), dtype=np.float32)
    for index in range(data.shape[0]):
        new_data = np.ndarray(shape=(1, image_size * image_size), dtype=np.float32)
        new_data = np.asarray([float(pixel) for pixel in data[index].split()]).reshape(1, -1)
        if preprocess:
            new_data = preprocess.fit_transform([new_data[0]])
        convert_data[index, :] = new_data
    return convert_data


# Create pickle file for the future use
def writePickle(train_data, train_labels, valid_data, valid_labels, test_data, test_labels, output_dir, preprocess_flag):
    if not os.path.isdir(output_dir):
        print("Create %s directory" % output_dir)
        os.makedirs(output_dir)

    print("Begin writing pickle file")
    pickle_file = os.path.join(output_dir, "facial_reg_{}.pickle".format(preprocess_flag))
    try:
        f = open(pickle_file, "wb")
        save = {
            "train_dataset": train_data,
            "train_labels": train_labels,
            "valid_dataset": valid_data,
            "valid_labels": valid_labels,
            "test_dataset": test_data,
            "test_labels": test_labels,
            }
        pickle.dump(save, f, pickle.HIGHEST_PROTOCOL)
        f.close()
    except Exception as e:
        print("Unable to save data to ", pickle_file, ':', e)
        raise


def preprocessData1(df, preprocess_flag=0):
    # Train dataset
    train_dataset = df[df["Usage"] == "Training"]
    train_data = convertData(train_dataset.loc[:, "pixels"], preprocess_flag)
    train_labels = train_dataset.loc[:, "emotion"].values.ravel()

    # Valid dataset
    valid_dataset = df[df.Usage == "PublicTest"]
    valid_dataset.reset_index(inplace=True)
    valid_data = convertData(valid_dataset.loc[:, "pixels"], preprocess_flag)
    valid_labels = valid_dataset.loc[:, "emotion"].values.ravel()

    # Test dataset
    test_dataset = df[df.Usage == "PrivateTest"]
    test_dataset.reset_index(inplace=True)
    test_data = convertData(test_dataset.loc[:, "pixels"], preprocess_flag)
    test_labels = test_dataset.loc[:, "emotion"].values.ravel()

    return train_data, train_labels, valid_data, valid_labels, test_data, test_labels


# Preprocessing like a winner in kaggle competition as given in the paper dlsvm.pdf
def preprocessData2(df):
    # Train dataset
    train_dataset = df[df["Usage"] == "Training"]
    train_data = convertData(train_dataset.loc[:, "pixels"])
    train_labels = train_dataset.loc[:, "emotion"].values.ravel()

    # Subtract the mean value of each image
    mean_img = np.mean(train_data, axis=1)
    train_data = train_data - mean_img.reshape((mean_img.shape[0]), 1)
    img_norm = 2.55 # Like a paper said 2.55 equals 255/100
    train_data /= img_norm

    # Each pixel is then standardize by subtracting its mean and dividing its
    # value by the standard deviation of that pixel
    pixels_mean = np.mean(train_data, axis=0)
    pixels_std = np.std(train_data, axis=0)
    train_data -= pixels_mean
    train_data /= pixels_std

    # Valid dataset
    valid_dataset = df[df["Usage"] == "PublicTest"]
    valid_dataset.reset_index(inplace=True)
    valid_data = convertData(valid_dataset.loc[:, "pixels"])
    valid_labels = valid_dataset.loc[:, "emotion"].values.ravel()
    mean_img = np.mean(valid_data, axis=1)
    valid_data = valid_data - mean_img.reshape((mean_img.shape[0]), 1)
    valid_data /= img_norm
    pixels_mean = np.mean(valid_data, axis=0)
    pixels_std = np.std(valid_data, axis=0)
    valid_data -= pixels_mean
    valid_data /= pixels_std

    # Test dataset
    test_dataset = df[df["Usage"] == "PrivateTest"]
    test_dataset.reset_index(inplace=True)
    test_data = convertData(test_dataset.loc[:, "pixels"])
    test_labels = test_dataset.loc[:, "emotion"].values.ravel()
    mean_img = np.mean(test_data, axis=1)
    test_data = test_data - mean_img.reshape((mean_img.shape[0]), 1)
    test_data /= img_norm
    pixels_mean = np.mean(test_data, axis=0)
    pixels_std = np.std(test_data, axis=0)
    test_data -= pixels_mean
    test_data /= pixels_std

    return train_data, train_labels, valid_data, valid_labels, test_data, test_labels

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("Wrong parameters")
        exit()

    # Parameters option
    input_file = sys.argv[1]
    output_dir = sys.argv[2]
    preprocess_flag = int(sys.argv[3])

    if not os.path.isfile(input_file):
        print("No file exists")
        exit()
    df = pd.read_csv(input_file)
    if preprocess_flag == 0:
        train_data, train_labels, valid_data, valid_labels, test_data, test_labels = preprocessData2(df)
        writePickle(train_data, train_labels, valid_data, valid_labels, test_data, test_labels, output_dir, preprocess_flag)
    else:
        train_data, train_labels, valid_data, valid_labels, test_data, test_labels = preprocessData1(df, preprocess_flag)
        writePickle(train_data, train_labels, valid_data, valid_labels, test_data, test_labels, output_dir, preprocess_flag)















